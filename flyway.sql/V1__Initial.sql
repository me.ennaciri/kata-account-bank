
CREATE TABLE account
(
    id INT PRIMARY KEY,
    first_name VARCHAR(20) NOT NULL,
    last_name VARCHAR(20) NOT NULL,
    balance DECIMAL NOT NULL
);

CREATE TABLE operation
(
    id INT PRIMARY KEY,
    account_id INTEGER NOT NULL REFERENCES account(id),
    type VARCHAR(20) NOT NULL,
    amount  DECIMAL NOT NULL,
    balance  DECIMAL NOT NULL,
    date DATE
);




