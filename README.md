# Bank account transactions
> When not specified, every command should be run from root directory

## Getting started
Prerequisites on local machine:
- Java 14 installed and the corresponding SDK
- Maven 3 installed
- Docker machine running



Step to run kata.application locally:
- Clone project: `git clone git@github.com:naciro2010/accountbank.git`
- Start PostgreSQL through Docker: `docker-compose up`
- build kata.application and run test: `.mvn clean install`


### Basic commands
Start docker containers:
- with logs in console: `docker-compose up`
- as daemon in background: `docker-compose up -d`
- stop daemon: `docker-compose stop`








-------------------------
# User Stories
##### US 1:
**In order to** save money  
**As a** bank client  
**I want to** make a deposit in my account  
 
##### US 2: 
**In order to** retrieve some or all of my savings  
**As a** bank client  
**I want to** make a withdrawal from my account  
 
##### US 3: 
**In order to** check my operations  
**As a** bank client  
**I want to** see the history (operation, date, amount, balance)  of my operations  