package kata.infra.adapter

import kata.domain.core.Account
import kata.domain.core.Amount
import kata.domain.core.Costumer
import kata.domain.core.Operation
import kata.domain.exception.AccountNotFound
import kata.domain.exception.OperationNotFound
import kata.infra.datasource.entity.AccountEntity
import kata.infra.datasource.entity.OperationEntity
import kata.infra.repository.AccountRepository
import kata.infra.repository.OperationRepository
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.Month

@RunWith(MockitoJUnitRunner::class)
class AccountOperationTest {
    @Mock
    private lateinit var accountRepository: AccountRepository

    @Mock
    private lateinit var operationRepository: OperationRepository

    @InjectMocks
    private lateinit var adapter: AccountOperationRepository


    private val now = LocalDateTime.of(
        LocalDate.of(2021, Month.FEBRUARY, 2),
        LocalTime.of(17, 0)
    )

    private val identifyAccount = 1931831L


    @Test
    fun get_Operations_by_Account() {
        given(operationRepository.findByAccountIdentify(identifyAccount)).willReturn(
            listOf(
                OperationEntity(
                    1L,
                    identifyAccount,
                    Operation.OperationType.DEPOSIT.toString(),
                    BigDecimal.valueOf(200),
                    BigDecimal.valueOf(200),
                    SimpleDateFormat("yyyy-MM-dd'T'HH:mm").parse("2021-02-02T17:00")
                ),

                OperationEntity(
                    2L,
                    identifyAccount,
                    Operation.OperationType.WITHDRAWAL.toString(),
                    BigDecimal.valueOf(300).negate(),
                    BigDecimal.valueOf(500),
                    SimpleDateFormat("yyyy-MM-dd'T'HH:mm").parse("2021-02-02T17:00")
                ),
                OperationEntity(
                    3L,
                    identifyAccount,
                    Operation.OperationType.DEPOSIT.toString(),
                    BigDecimal.valueOf(200),
                    BigDecimal.valueOf(300),
                    SimpleDateFormat("yyyy-MM-dd'T'HH:mm").parse("2021-02-02T17:00")
                )
            )

        )

        assertThat(adapter.getOperationsByAccountIdentify(identifyAccount)).usingRecursiveComparison().isEqualTo(
            listOf(
                Operation(
                    identifyAccount,
                    Operation.OperationType.DEPOSIT,
                    Amount(BigDecimal.valueOf(200)),
                    Amount(BigDecimal.valueOf(200)),
                    now
                ),
                Operation(
                    identifyAccount,
                    Operation.OperationType.WITHDRAWAL,
                    Amount(BigDecimal.valueOf(300).negate()),
                    Amount(BigDecimal.valueOf(500)),
                    now
                ),
                Operation(
                    identifyAccount,
                    Operation.OperationType.DEPOSIT,
                    Amount(BigDecimal.valueOf(200)),
                    Amount(BigDecimal.valueOf(300)),
                    now
                )
            )
        )
    }

    @Test
    fun get_a_account_by_identify() {
        given(accountRepository.findAccountEntityById(identifyAccount)).willReturn(
            AccountEntity(identifyAccount, "firstName", "lastName", BigDecimal.valueOf(100))
        )
        assertEquals(
            adapter.getAccountByAccountIdentify(identifyAccount),
            Account(identifyAccount, Costumer("firstName", "lastName"), Amount(BigDecimal.valueOf(100)))
        )

    }

    @Test
    fun throw_exception_when_account_not_found() {
        given(accountRepository.findAccountEntityById(identifyAccount)).willReturn(
            null
        )
        Assertions.assertThatThrownBy {
            adapter.getAccountByAccountIdentify(identifyAccount)
        }.isInstanceOf(AccountNotFound::class.java)

    }

}