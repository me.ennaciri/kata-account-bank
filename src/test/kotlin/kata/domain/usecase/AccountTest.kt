package kata.domain.usecase

import kata.domain.core.*
import kata.domain.exception.CurrencyNotValid
import kata.domain.exception.OverdraftNotAuthorized
import kata.infra.adapter.AccountOperationRepository
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.Month

@RunWith(MockitoJUnitRunner::class)
class AccountTest {

    @Mock
    private lateinit var accountRepository: AccountOperationRepository

    @InjectMocks
    private lateinit var useCase: AccountUseCase

    private val now = LocalDateTime.of(
        LocalDate.of(2021, Month.FEBRUARY, 2),
        LocalTime.of(17, 9)
    )

    private val identifyAccount = 1312233L

    private fun accountDummy(newBalance: Amount? = null) = Account(
        identifyAccount,
        Costumer("firstname", "lastname"),
        balance = newBalance?.let { newBalance } ?: Amount(BigDecimal.valueOf(0))
    )


    @Test
    fun add_a_deposit_operation() {

        given(accountRepository.getAccountByAccountIdentify(identifyAccount)).willReturn(
            accountDummy()
        )
        given(accountRepository.postAccount(accountDummy(Amount(BigDecimal.valueOf(200))))).willReturn(
            accountDummy(Amount(BigDecimal.valueOf(200)))
        )

        assertEquals(
            useCase.deposit(
                Amount(BigDecimal.valueOf(200)), identifyAccount
            ), accountDummy(Amount(BigDecimal.valueOf(200)))
        )

    }


    @Test
    fun add_a_withdraw_operation() {
        given(accountRepository.getAccountByAccountIdentify(identifyAccount)).willReturn(
            accountDummy(Amount(BigDecimal.valueOf(200)))
        )
        given(accountRepository.postAccount(accountDummy())).willReturn(
            accountDummy()
        )
        assertEquals(
            useCase.withdraw(
                Amount(BigDecimal.valueOf(200)), identifyAccount
            ), accountDummy(Amount(BigDecimal.valueOf(0)))
        )


    }

    @Test
    fun throw_exception_when_add_a_deposit_with_currency_not_correct() {

        given(accountRepository.getAccountByAccountIdentify(identifyAccount)).willReturn(
            accountDummy()
        )
        assertThatThrownBy {
            useCase.deposit(Amount(BigDecimal.valueOf(200), "USD"), identifyAccount)
        }.isInstanceOf(CurrencyNotValid::class.java)
    }

    @Test
    fun throw_exception_when_add_a_withdraw_with_currency_not_correct() {

        given(accountRepository.getAccountByAccountIdentify(identifyAccount)).willReturn(
            accountDummy()
        )
        assertThatThrownBy {
            useCase.withdraw(Amount(BigDecimal.valueOf(200), "USD"), identifyAccount)
        }.isInstanceOf(CurrencyNotValid::class.java)
    }

    @Test
    fun throw_exception_when_balance_less_to_zero() {

        given(accountRepository.getAccountByAccountIdentify(identifyAccount)).willReturn(
            accountDummy()
        )
        assertThatThrownBy {
            useCase.withdraw(Amount(BigDecimal.valueOf(200)), identifyAccount)
        }.isInstanceOf(OverdraftNotAuthorized::class.java)
    }

    @Test
    fun print_statement_operations() {

        given(accountRepository.getOperationsByAccountIdentify(identifyAccount)).willReturn(
            listOf(
                Operation(
                    identifyAccount,
                    Operation.OperationType.WITHDRAWAL,
                    Amount(BigDecimal.valueOf(100)).toNegate(),
                    Amount(BigDecimal.valueOf(300)),
                    now
                ),
                Operation(
                    identifyAccount,
                    Operation.OperationType.DEPOSIT,
                    Amount(BigDecimal.valueOf(400)).toNegate(),
                    Amount(BigDecimal.valueOf(700)),
                    now
                )
            )
        )

        assertEquals(
            useCase.showHistory(identifyAccount), listOf(
                Operation(
                    identifyAccount,
                    Operation.OperationType.WITHDRAWAL,
                    Amount(BigDecimal.valueOf(100)).toNegate(),
                    Amount(BigDecimal.valueOf(300)),
                    now
                ),
                Operation(
                    identifyAccount,
                    Operation.OperationType.DEPOSIT,
                    Amount(BigDecimal.valueOf(400)).toNegate(),
                    Amount(BigDecimal.valueOf(700)),
                    now
                )
            )
        )

    }
}