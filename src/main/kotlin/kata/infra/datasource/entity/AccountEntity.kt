package kata.infra.datasource.entity

import java.math.BigDecimal
import javax.persistence.*

@Entity
@Table(name = "account")
data class AccountEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,
    val firstName: String,
    val lastName: String,
    val balance: BigDecimal
)
