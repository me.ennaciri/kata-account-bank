package kata.infra.datasource.entity

import java.math.BigDecimal
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "operation")
class OperationEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,
    val accountIdentify: Long,
    val type: String,
    val amount: BigDecimal,
    val balance: BigDecimal,
    val date: Date
)

