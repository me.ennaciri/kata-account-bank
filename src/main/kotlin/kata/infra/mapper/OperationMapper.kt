package kata.infra.mapper

import kata.domain.core.Amount
import kata.domain.core.Operation
import kata.domain.core.toNegate
import kata.infra.datasource.entity.OperationEntity
import java.sql.Date
import java.sql.Timestamp
import java.time.ZoneOffset

fun OperationEntity.toDomain() = id?.let {
    Operation(
        accountIdentify,
        Operation.OperationType.valueOf(type),
        Amount(amount),
        balance = Amount(balance),
        date = Timestamp(date.time).toLocalDateTime()
    )
}

fun Operation.toRepository() = OperationEntity(
    accountIdentify = accountIdentify,
    type = operationType.toString(),
    balance = balance.amount,
    amount =
    when (operationType) {
        Operation.OperationType.WITHDRAWAL -> amount.toNegate().amount
        Operation.OperationType.DEPOSIT -> amount.amount
    },
    date = Date.from(date.toInstant(ZoneOffset.UTC))
)

