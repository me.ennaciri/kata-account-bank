package kata.infra.mapper

import kata.domain.core.Account
import kata.domain.core.Amount
import kata.domain.core.Costumer
import kata.infra.datasource.entity.AccountEntity
import java.math.BigDecimal

data class AccountDto(val identify: String, val firstName: String, val lastName: String, val balance: BigDecimal)

fun AccountEntity.toDomain() =
    id?.let { Account(it, Costumer(firstName, lastName), Amount(balance)) }

fun Account.toRepository() =
    AccountEntity(firstName = costumer.firstName, lastName = costumer.lastName, balance = balance.amount)

