package kata.infra.adapter

import kata.domain.core.Account
import kata.domain.core.Operation
import kata.domain.exception.AccountNotFound
import kata.domain.exception.OperationNotFound
import kata.domain.port.IAccountRepository
import kata.infra.mapper.toDomain
import kata.infra.mapper.toRepository
import kata.infra.repository.AccountRepository
import kata.infra.repository.OperationRepository
import org.springframework.stereotype.Component

@Component
class AccountOperationRepository(
    private val accountRepository: AccountRepository,
    private val operationRepository: OperationRepository
) :
    IAccountRepository {
    override fun getOperationsByAccountIdentify(accountIdentify: Long): List<Operation> {
        return operationRepository.findByAccountIdentify(accountIdentify)
            .map {
                it ?.let { it.toDomain() }
                ?: throw OperationNotFound(
                    accountId = accountIdentify
                )
        }
    }

    override fun getAccountByAccountIdentify(accountIdentify: Long): Account {
        return accountRepository.findAccountEntityById(accountIdentify)
                ?.let { it.toDomain() }
                ?: throw AccountNotFound(
                    accountId = accountIdentify
                )
    }

    override fun postAccount(account: Account) : Account{
        return accountRepository.save(account.toRepository())?.let { it.toDomain() }
            ?: throw AccountNotFound(
                accountId = account.identify
            )
    }

    override fun postOperation(operation: Operation) {
        operationRepository.save(operation.toRepository())
    }
}


