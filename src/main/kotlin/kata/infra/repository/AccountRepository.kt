package kata.infra.repository


import kata.infra.datasource.entity.AccountEntity
import kata.infra.datasource.entity.OperationEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Repository
@Transactional(Transactional.TxType.MANDATORY)
interface AccountRepository : JpaRepository<AccountEntity, Long>, JpaSpecificationExecutor<AccountEntity>{
    fun findAccountEntityById(accountIdentify : Long) : AccountEntity

}