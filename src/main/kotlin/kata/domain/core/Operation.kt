package kata.domain.core

import java.time.LocalDateTime


data class Operation(
    val accountIdentify: Long,
    val operationType: OperationType,
    var amount: Amount,
    val balance: Amount,
    val date: LocalDateTime = LocalDateTime.now()
) {
    enum class OperationType {
        DEPOSIT, WITHDRAWAL
    }

    override fun toString(): String =
        "Operation : ( " +
                "account identify = $accountIdentify " +
                "Operation Type = $operationType " +
                "amount = $amount " +
                "balance = $balance " +
                "date = $date " +

                ")"
}

