package kata.domain.core

import kata.domain.exception.OverdraftNotAuthorized

data class Account(
    val identify: Long,
    val costumer: Costumer,
    val balance: Amount
){
    init {
        if (balance.isNegate())
            throw OverdraftNotAuthorized(
                identify
            )
    }
}
