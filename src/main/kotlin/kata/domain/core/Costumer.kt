package kata.domain.core

data class Costumer(
    val firstName: String,
    val lastName: String
)