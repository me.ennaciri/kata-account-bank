package kata.domain.core

import kata.domain.exception.CurrencyNotValid
import java.math.BigDecimal

data class Amount(
    val amount: BigDecimal,
    val currency: String = "EUR"

)

fun Amount.add(otherAmount: Amount): Amount {
    if (currency == otherAmount.currency)
        return Amount(amount.add(otherAmount.amount))
    else
        throw CurrencyNotValid(
            otherAmount.currency
        )
}

fun Amount.subtract(otherAmount: Amount): Amount {
    if (currency == otherAmount.currency)
        return Amount(amount.subtract(otherAmount.amount))
    else
        throw CurrencyNotValid(
            otherAmount.currency
        )
}

fun Amount.toNegate() = Amount(amount.negate())

fun Amount.isNegate() = amount < BigDecimal.valueOf(0)

