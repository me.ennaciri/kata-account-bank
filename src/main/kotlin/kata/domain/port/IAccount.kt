package kata.domain.port

import kata.domain.core.Account
import kata.domain.core.Amount
import kata.domain.core.Operation

interface IAccount {
    fun deposit(amount: Amount, accountIdentify: Long) : Account
    fun withdraw(amount: Amount, accountIdentify: Long) : Account
    fun showHistory(accountIdentify: Long): List<Operation>
}


