package kata.domain.port

import kata.domain.core.Account
import kata.domain.core.Operation

interface IAccountRepository {
    fun getOperationsByAccountIdentify(accountIdentify: Long): List<Operation>
    fun getAccountByAccountIdentify(accountIdentify: Long): Account
    fun postAccount(account: Account) : Account
    fun postOperation(operation: Operation)
}