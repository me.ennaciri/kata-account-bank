package kata.domain.usecase

import kata.domain.core.*
import kata.domain.port.IAccount
import kata.domain.port.IAccountRepository
import org.springframework.stereotype.Component

@Component
class AccountUseCase(private val accountRepository: IAccountRepository) : IAccount {
    override fun deposit(amount: Amount, accountIdentify: Long): Account {
        var newBalance: Amount

        accountRepository.getAccountByAccountIdentify(accountIdentify).let {
            accountRepository.postAccount(Account(accountIdentify, it.costumer, it.balance.add(amount)))
        }.also {
            accountRepository.postOperation(
                Operation(
                    it.identify,
                    Operation.OperationType.DEPOSIT,
                    amount,
                    it.balance
                )
            )
        }.let { return it }
    }

    override fun withdraw(amount: Amount, accountIdentify: Long): Account {
        accountRepository.getAccountByAccountIdentify(accountIdentify).let {
            accountRepository.postAccount(Account(accountIdentify, it.costumer, it.balance.subtract(amount)))
        }.also {
            accountRepository.postOperation(
                Operation(
                    it.identify,
                    Operation.OperationType.WITHDRAWAL,
                    amount,
                    it.balance
                )
            )
        }.let { return it }

    }

    override fun showHistory(accountIdentify: Long): List<Operation> {
        return accountRepository.getOperationsByAccountIdentify(accountIdentify)
    }

}