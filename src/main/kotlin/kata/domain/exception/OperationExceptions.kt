package kata.domain.exception

import kata.domain.core.Amount

class OperationNotAuthorized(
    accountId: String,
    operationType: String,
    amount: Amount
) : AccountBankException("unauthorized operation for the client '$accountId' for type '$operationType' for amount '$amount' ")

class CurrencyNotValid(
    currency: String
) : AccountBankException("unauthorized operation for this currency '$currency'")

class OverdraftNotAuthorized(
    accountId: Long
) : AccountBankException("unauthorized operation for the client '$accountId' ")

class OperationNotFound(
    accountId: Long,
    cause: Throwable? = null
) : AccountBankException("no Operation found for Account id '$accountId'", cause)




