package kata.domain.exception

class AccountNotFound (
    accountId: Long,
    cause: Throwable? = null
) : AccountBankException("Account with id '$accountId' not found", cause)
