package kata.application

import kata.domain.core.Amount
import kata.domain.port.IAccount
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import java.math.BigDecimal


@SpringBootApplication(scanBasePackages = ["kata"])
open class StartApplication : CommandLineRunner {

    @Autowired
    lateinit var accountUseCase: IAccount
    override fun run(vararg args: String) {

        accountUseCase.deposit(Amount(BigDecimal.valueOf(30)), 1)
        accountUseCase.deposit(Amount(BigDecimal.valueOf(40)), 1)
        accountUseCase.deposit(Amount(BigDecimal.valueOf(30)), 1)
        accountUseCase.withdraw(Amount(BigDecimal.valueOf(30)), 1)
        accountUseCase.withdraw(Amount(BigDecimal.valueOf(10)), 1)

        println("\nShow history()")
        accountUseCase.showHistory(1).map {
            it.toString()
        }
    }


    fun main(args: Array<String>) {
        runApplication<StartApplication>(*args)
    }
}